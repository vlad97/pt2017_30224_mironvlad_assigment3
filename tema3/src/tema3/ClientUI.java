package tema3;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

public class ClientUI extends JFrame {

	   private JTextField idField = new JTextField(10);
	   private JTextField fNameField = new JTextField(30);
	   private JTextField adresaField = new JTextField(30);
	   private JTextField emailField = new JTextField(30);
	   private JTextField varstaField = new JTextField(30);


	   private JButton createButton = new JButton("Create");
	   private JButton updateButton = new JButton("Update");
	   private JButton deleteButton = new JButton("Delete");
	   private JButton firstButton = new JButton("First");
	   private JButton prevButton = new JButton("Prev");
	   private JButton nextButton = new JButton("Next");
	   private JButton lastButton = new JButton("Last");
	  
	   private ClientAdministration bean = new ClientAdministration();

	   public ClientUI() {
		   JPanel p = new JPanel();
	      p.setBorder(new TitledBorder
	      (new EtchedBorder(),"Client Details"));
	      p.setLayout(new BorderLayout(5, 5));
	      p.add(initFields(), BorderLayout.NORTH);
	      p.add(initButtons(), BorderLayout.CENTER);
	      setFieldData(bean.moveFirst());
	      setContentPane(p);
	      setSize(600,300);
	      setLocation(200,200);
	      setVisible(true);
	   }

	   private JPanel initButtons() {
	      JPanel panel = new JPanel();
	      panel.setLayout(new FlowLayout(FlowLayout.CENTER, 3, 3));
	      panel.add(createButton);
	      createButton.addActionListener(new ButtonHandler());
	      
	      panel.add(lastButton);
	      lastButton.addActionListener(new ButtonHandler());
	      panel.add(updateButton);
	      updateButton.addActionListener(new ButtonHandler());
	      panel.add(deleteButton);
	      deleteButton.addActionListener(new ButtonHandler());
	      panel.add(firstButton);
	      firstButton.addActionListener(new ButtonHandler());
	      panel.add(prevButton);
	      prevButton.addActionListener(new ButtonHandler());
	      panel.add(nextButton);
	      nextButton.addActionListener(new ButtonHandler());
	      return panel;
	   }

	   private JPanel initFields() {
	      JPanel panel = new JPanel();
	      panel.setLayout(new GridLayout(5,2));
	      panel.add(new JLabel("ID"), "align label");
	      panel.add(idField, "wrap");
	      idField.setEnabled(false);
	      panel.add(new JLabel("Nume"), "align label");
	      panel.add(fNameField, "wrap");
	      //...
	      panel.add(new JLabel("Adresa"), "align label");
	      panel.add(adresaField, "wrap");
	      panel.add(new JLabel("Email"), "align label");
	      panel.add(emailField, "wrap");
	      panel.add(new JLabel("Varsta"), "align label");
	      panel.add(varstaField, "wrap");
	      return panel;
	   }

	   private Client getFieldData() {
	      Client c = new Client();
	      if (!idField.getText().trim().isEmpty())
	    	  c.setId(Integer.parseInt(idField.getText()));
	      c.setNume(fNameField.getText());
	      c.setAdresa(adresaField.getText());
	      c.setEmail(emailField.getText());
	      if (!varstaField.getText().trim().isEmpty())
	    	  c.setVarsta(Integer.parseInt(varstaField.getText()));
	      return c;
	   }

	   private void setFieldData(Client c) {
	      idField.setText(String.valueOf(c.getId()));
	      fNameField.setText(c.getNume());
	      adresaField.setText(c.getAdresa());
	      emailField.setText(c.getEmail());
	      varstaField.setText(c.getVarsta()!=0?String.valueOf(c.getVarsta()):"");
	   }

	   private boolean isInvalidFieldData() {
	      return (fNameField.getText().trim().isEmpty()
	         || adresaField.getText().trim().isEmpty()
	         || emailField.getText().trim().isEmpty()
	         || varstaField.getText().trim().isEmpty() || Integer.parseInt(varstaField.getText())<=0);
	   }

	   private class ButtonHandler implements ActionListener {
	      
	      public void actionPerformed(ActionEvent e) {
	         Client c = getFieldData();
	         switch (e.getActionCommand()) {
	         case "Save":
	            if (isInvalidFieldData()) {
	               JOptionPane.showMessageDialog(null,
	               "Cannot create an invalid record");
	               return;
	            }
	            if (bean.create(c) != null)
	               JOptionPane.showMessageDialog(null,
	               "New person created successfully.");
	               createButton.setText("Create");
	               setFieldData(bean.moveLast());
	               break;
	         case "Create":	        	
	            c.setNume("");
	            c.setAdresa("");
	            c.setEmail("");	            
	            c.setVarsta(0);
	            setFieldData(c);
	        	idField.setText("");
	            createButton.setText("Save");
	            break;
	         case "Update":
	            if (isInvalidFieldData()) {
	               JOptionPane.showMessageDialog(null,
	               "Cannot update an invalid record");
	               return;
	            }
	            if (bean.update(c) != null)
	               JOptionPane.showMessageDialog(
	               null,"Person with ID:" + String.valueOf(c.getId()
	               + " is updated successfully"));
	               break;
	         case "Delete":
	            if (isInvalidFieldData()) {
	               JOptionPane.showMessageDialog(null,
	               "Cannot delete an invalid record");
	               return;
	            }
	            c = bean.getCurrent();
	            bean.delete();
	            JOptionPane.showMessageDialog(
	               null,"Person with ID:"
	               + String.valueOf(c.getId()
	               + " is deleted successfully"));
	            setFieldData(bean.moveLast());
	               break;
	         case "First":
	            setFieldData(bean.moveFirst()); break;
	         case "Prev":
	            setFieldData(bean.movePrevious()); break;
	         case "Next":
	            setFieldData(bean.moveNext()); break;
	         case "Last":
	            setFieldData(bean.moveLast()); break;
	         default:
	            JOptionPane.showMessageDialog(null,
	            "invalid command");
	         }
	      }

		
	   }
	}