package tema3;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class MainUI extends JFrame{
	
	private MainUI()	
	{
		JPanel panel = new JPanel();
		JButton clients = new JButton("CLIENTS");
		JButton products = new JButton("PRODUCTS");
		JButton orders = new JButton("ORDERS");
		panel.add(clients);
		panel.add(products);
		panel.add(orders);
		orders.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				OrderUI oUI=new OrderUI();
			}	
		});
		
		clients.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				new ClientUI();
			}	
		});
		
		
		products.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				new ProductUI();
			}	
		});
		
		
		
		
		
	    
		setTitle("WAREHOUSE APPLICATION");
	    getContentPane().setLayout(new FlowLayout(FlowLayout.CENTER));
	    getContentPane().add(panel);
		setSize(500,90);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
		
	
	
	   public static void main(String[] args) {
		   	new MainUI();		   
	   }
	}