package tema3;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

public class ProductUI extends JFrame {

	   private JTextField idField = new JTextField(10);
	   private JTextField pnumeField = new JTextField(30);
	   private JTextField cantitateField = new JTextField(30);
	   private JTextField pretField = new JTextField(30);
	  


	   private JButton createButton = new JButton("Create");
	   private JButton updateButton = new JButton("Update");
	   private JButton deleteButton = new JButton("Delete");
	   private JButton firstButton = new JButton("First");
	   private JButton prevButton = new JButton("Prev");
	   private JButton nextButton = new JButton("Next");
	   private JButton lastButton = new JButton("Last");
	  
	   private ProductAdministration bean = new ProductAdministration();

	   public ProductUI() {
		   JPanel p = new JPanel();
	      p.setBorder(new TitledBorder
	      (new EtchedBorder(),"Product Details"));
	      p.setLayout(new BorderLayout(5, 5));
	      p.add(initFields(), BorderLayout.NORTH);
	      p.add(initButtons(), BorderLayout.CENTER);
	      setFieldData(bean.moveFirst());
	      setContentPane(p);
	      setSize(600,300);
	      setLocation(200,200);
	      setVisible(true);
	   }

	   private JPanel initButtons() {
	      JPanel panel = new JPanel();
	      panel.setLayout(new FlowLayout(FlowLayout.CENTER, 3, 3));
	      panel.add(createButton);
	      createButton.addActionListener(new ButtonHandler());
	      
	      panel.add(lastButton);
	      lastButton.addActionListener(new ButtonHandler());
	      panel.add(updateButton);
	      updateButton.addActionListener(new ButtonHandler());
	      panel.add(deleteButton);
	      deleteButton.addActionListener(new ButtonHandler());
	      panel.add(firstButton);
	      firstButton.addActionListener(new ButtonHandler());
	      panel.add(prevButton);
	      prevButton.addActionListener(new ButtonHandler());
	      panel.add(nextButton);
	      nextButton.addActionListener(new ButtonHandler());
	      return panel;
	   }

	   private JPanel initFields() {
	      JPanel panel = new JPanel();
	      panel.setLayout(new GridLayout(5,2));
	      panel.add(new JLabel("ID"), "align label");
	      panel.add(idField, "wrap");
	      idField.setEnabled(false);
	      panel.add(new JLabel("Nume"), "align label");
	      panel.add(pnumeField, "wrap");
	      //...
	      panel.add(new JLabel("Cantitate"), "align label");
	      panel.add(cantitateField, "wrap");
	      panel.add(new JLabel("Pret"), "align label");
	      panel.add(pretField, "wrap");
	     
	      return panel;
	   }

	   private Product getFieldData() {
	      Product p = new Product();
	      if (!idField.getText().trim().isEmpty())
	    	  p.setId(Integer.parseInt(idField.getText()));
	      p.setPnume(pnumeField.getText());
	      p.setCantitate(Integer.parseInt(cantitateField.getText()));
	      p.setPret(Integer.parseInt(pretField.getText()));
	      
	      return p;
	   }

	   private void setFieldData(Product p) {
	      idField.setText(String.valueOf(p.getId()));
	      pnumeField.setText(p.getPnume());
	      cantitateField.setText(String.valueOf(p.getCantitate()));
	      pretField.setText(String.valueOf(p.getPret()));
	    
	   }

	   private boolean isInvalidFieldData() {
	      return (pnumeField.getText().trim().isEmpty()
	         || cantitateField.getText().trim().isEmpty()
	         || pretField.getText().trim().isEmpty()
	         );
	   }

	   private class ButtonHandler implements ActionListener {
	      
	      public void actionPerformed(ActionEvent e) {
	         Product p = getFieldData();
	         switch (e.getActionCommand()) {
	         case "Save":
	            if (isInvalidFieldData()) {
	               JOptionPane.showMessageDialog(null,
	               "Cannot create an invalid record");
	               return;
	            }
	            if (bean.create(p) != null)
	               JOptionPane.showMessageDialog(null,
	               "New product created successfully.");
	               createButton.setText("Create");
	               setFieldData(bean.moveLast());
	               break;
	         case "Create":	        	
	            p.setPnume("");
	            p.setCantitate(0);
	            p.setPret(0);	            
	            setFieldData(p);
	        	idField.setText("");
	            createButton.setText("Save");
	            break;
	         case "Update":
	            if (isInvalidFieldData()) {
	               JOptionPane.showMessageDialog(null,
	               "Cannot update an invalid record");
	               return;
	            }
	            if (bean.update(p) != null)
	               JOptionPane.showMessageDialog(
	               null,"Product with ID:" + String.valueOf(p.getId()
	               + " is updated successfully"));
	               break;
	         case "Delete":
	            if (isInvalidFieldData()) {
	               JOptionPane.showMessageDialog(null,
	               "Cannot delete an invalid record");
	               return;
	            }
	            p = bean.getCurrent();
	            bean.delete();
	            JOptionPane.showMessageDialog(
	               null,"Product with ID:"
	               + String.valueOf(p.getId()
	               + " is deleted successfully"));
	            setFieldData(bean.moveLast());
	               break;
	         case "First":
	            setFieldData(bean.moveFirst()); break;
	         case "Prev":
	            setFieldData(bean.movePrevious()); break;
	         case "Next":
	            setFieldData(bean.moveNext()); break;
	         case "Last":
	            setFieldData(bean.moveLast()); break;
	         default:
	            JOptionPane.showMessageDialog(null,
	            "invalid command");
	         }
	      }

		
	   }
	}