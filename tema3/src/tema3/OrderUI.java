package tema3;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

public class OrderUI extends JFrame {

	   private JComboBox clientField = new JComboBox();
	   private JComboBox productField = new JComboBox();
	   private JTextField quantityField = new JTextField(30);

	   private JButton addButton = new JButton("Add order");
	  
	   private OrderAdministration bean = new OrderAdministration();

	   public OrderUI() {
		   JPanel p = new JPanel();
	      p.setBorder(new TitledBorder
	      (new EtchedBorder(),"Order Details"));
	      p.setLayout(new BorderLayout(5, 5));
	      p.add(initFields(), BorderLayout.NORTH);
	      p.add(initButtons(), BorderLayout.CENTER);
	      setFieldData();
	      p.revalidate();
	      setContentPane(p);
	      setSize(300,240);
	      setLocation(200,200);
	      setVisible(true);
	   }

	   private JPanel initButtons() {
	      JPanel panel = new JPanel();
	      panel.setLayout(new FlowLayout(FlowLayout.CENTER, 3, 3));
	      panel.add(addButton);
	      addButton.addActionListener(new ButtonHandler());
	      return panel;
	   }

	   private JPanel initFields() {
	      JPanel panel = new JPanel();
	      panel.setLayout(new GridLayout(5,2));
	      panel.add(new JLabel("Client"), "align label");
	      panel.add(clientField, "wrap");
	      panel.add(new JLabel("Product"), "align label");
	      panel.add(productField, "wrap");
	      panel.add(new JLabel("Quantity"), "align label");
	      panel.add(quantityField, "wrap");

	      return panel;
	   }

	   private void setFieldData() {
		   clientField.removeAllItems();
		   for (String s: bean.getAllClients())
			   clientField.addItem(s);
		   productField.removeAllItems();
		   for (String s: bean.getAllProducts())
			   productField.addItem(s);
	
	   }
	   
	   private void placeOrder()
	   {
		   String c = (String)clientField.getSelectedItem();
		   String [] sc = c.split(":");
		   String p = (String)productField.getSelectedItem();
		   String [] sp = p.split(":");		   
		   int idC=Integer.parseInt(sc[0]);
		   int idP=Integer.parseInt(sp[0]);
		   ProductAdministration pa = new ProductAdministration();
		   int quantityAvailable = pa.getQuantity(idP);
		   try
		   {
			   int quantityRequested = Integer.parseInt(quantityField.getText());
			   if (quantityRequested>quantityAvailable)
				   JOptionPane.showMessageDialog(null, "maximum quantity available is "+quantityAvailable);
			   else
			   {
				   pa.setQuantity(idP, quantityAvailable-quantityRequested);
				   bean.insertOrder(idC, idP, quantityRequested);
				   JOptionPane.showMessageDialog(null, "order successfully placed!");
				   dispose();
			   }
			   
		   }catch (Exception e)
		   {
	            JOptionPane.showMessageDialog(null, "invalid quantity");
		   }
	   }

	   private boolean isInvalidFieldData() {
		return false;
	   }

	   private class ButtonHandler implements ActionListener {
	      
	      public void actionPerformed(ActionEvent e) {
	         switch (e.getActionCommand()) {
	         case "Add order":
	        	 placeOrder();
	        	 break;
	         default:
	            JOptionPane.showMessageDialog(null,
	            "invalid command");
	         }
	      }

		
	   }
	}