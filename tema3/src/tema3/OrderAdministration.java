package tema3;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

public class OrderAdministration {

	public void insertOrder(int cId, int pId, int quantity) {
		Connection conn= ConnectionFactory.getConnection();
		String insertStatement =  "INSERT INTO `ORDER` "
				+ "(C, P, CANTITATE) VALUES"
				+ "(?,?,?);";

		try {
			PreparedStatement preparedStatement = conn.prepareStatement(insertStatement);
			preparedStatement = conn.prepareStatement(insertStatement);
			preparedStatement.setInt(1, cId);
			preparedStatement.setInt(2, pId);
			preparedStatement.setInt(3, quantity);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Vector<String> getAllClients() {
		Connection conn= ConnectionFactory.getConnection();
		Vector<String> v = new Vector<String>();
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM CLIENT");
			while (rs.next()) {
				  int id = rs.getInt("id");
				  String name = rs.getString("nume");
				  String s = ""+id+":"+name;
				  v.add(s);
				}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return v;
	}	
	
	public Vector<String> getAllProducts() {
		Connection conn= ConnectionFactory.getConnection();
		Vector<String> v = new Vector<String>();
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM PRODUCT");
			while (rs.next()) {
				  int id = rs.getInt("id");
				  String name = rs.getString("pnume");
				  String s = ""+id+":"+name;
				  v.add(s);
				}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return v;
	}	

}
