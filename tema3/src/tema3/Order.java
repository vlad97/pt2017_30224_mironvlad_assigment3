package tema3;

public class Order {

	
	private int ID;
	private Product p;
	private Client c;
	
	
	public Order(Product p,Client c) {
		this.p=p;
		this.c=c;
	
	}
	
	public Product getP(){
		return p;
	}
	
	public void setP(Product p){
		this.p=p;
	}
	
	public Client getC(){
		return c;
	}
	
	public void setClient(Client c){
		this.c=c;
	}
	
	
	
	public int getIDOrder(){
		return ID;
	}

}
