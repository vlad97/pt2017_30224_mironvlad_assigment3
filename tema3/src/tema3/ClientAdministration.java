package tema3;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.rowset.JdbcRowSet;
import com.sun.rowset.*;

public class ClientAdministration {
	   private JdbcRowSet rowSet = null;
	   public ClientAdministration() {
	    	 rowSet=ConnectionFactory.createRowSet();
	         try {
				rowSet.setCommand("SELECT * FROM client");
				rowSet.execute();
			} catch (SQLException e) {
				e.printStackTrace();
			}
	   }
	   public Client create(Client c) {
	      try {
	         rowSet.moveToInsertRow();
	         //rowSet.updateInt("id", c.getId());
	         rowSet.updateString("Nume", c.getNume());
	         rowSet.updateString("adresa", c.getAdresa());
	         rowSet.updateString("email", c.getEmail());
	         rowSet.updateInt("varsta",c.getVarsta());
	         rowSet.insertRow();
	         rowSet.moveToCurrentRow();
	      } catch (SQLException ex) {
	         try {
	            rowSet.rollback();
	            c = null;
	         } catch (SQLException e) {

	         }
	         ex.printStackTrace();
	      }
	      return c;
	   }

	   public Client update(Client c) {
	      try {
	         rowSet.updateInt("id", c.getId());
	         rowSet.updateString("Nume", c.getNume());
	         rowSet.updateString("adresa", c.getAdresa());
	         rowSet.updateString("email", c.getEmail());
	         rowSet.updateInt("varsta",c.getVarsta());
	         rowSet.updateRow();
	         rowSet.moveToCurrentRow();
	      } catch (SQLException ex) {
	         try {
	            rowSet.rollback();
	         } catch (SQLException e) {

	         }
	         ex.printStackTrace();
	      }
	      return c;
	   }

	   public void delete() {
	      try {
	         rowSet.moveToCurrentRow();
	         rowSet.deleteRow();
	      } catch (SQLException ex) {
	         try {
	            rowSet.rollback();
	         } catch (SQLException e) { }
	         ex.printStackTrace();
	      }

	   }

	   public Client moveFirst() {
	      Client c = new Client();
	      try {
	         rowSet.first();
	         c.setId(rowSet.getInt("id"));
	         c.setNume(rowSet.getString("Nume"));
	         c.setAdresa(rowSet.getString("Adresa"));
	         c.setEmail(rowSet.getString("Email"));
	         c.setVarsta(rowSet.getInt("varsta"));

	      } catch (SQLException ex) {
	         ex.printStackTrace();
	      }
	      return c;
	   }

	   public Client moveLast() {
	      Client c = new Client();
	      try {
	         rowSet.last();
	         c.setId(rowSet.getInt("id"));
	         c.setNume(rowSet.getString("Nume"));
	         c.setAdresa(rowSet.getString("Adresa"));
	         c.setEmail(rowSet.getString("Email"));
	         c.setVarsta(rowSet.getInt("varsta"));

	      } catch (SQLException ex) {
	         ex.printStackTrace();
	      }
	      return c;
	   }

	   public Client moveNext() {
	      Client c = new Client();
	      try {
	         if (rowSet.next() == false)
	            rowSet.previous();
	         c.setId(rowSet.getInt("id"));
	         c.setNume(rowSet.getString("Nume"));
	         c.setAdresa(rowSet.getString("Adresa"));
	         c.setEmail(rowSet.getString("Email"));
	         c.setVarsta(rowSet.getInt("varsta"));

	      } catch (SQLException ex) {
	         ex.printStackTrace();
	      }
	      return c;
	   }

	   public Client movePrevious() {
	      Client c = new Client();
	      try {
	         if (rowSet.previous() == false)
	            rowSet.next();
	         c.setId(rowSet.getInt("id"));
	         c.setNume(rowSet.getString("Nume"));
	         c.setAdresa(rowSet.getString("Adresa"));
	         c.setEmail(rowSet.getString("Email"));
	         c.setVarsta(rowSet.getInt("varsta"));

	      } catch (SQLException ex) {
	         ex.printStackTrace();
	      }
	      return c;
	   }

	   public Client getCurrent() {
	      Client c = new Client();
	      try {
	         rowSet.moveToCurrentRow();
	         c.setId(rowSet.getInt("id"));
	         c.setNume(rowSet.getString("Nume"));
	         c.setAdresa(rowSet.getString("Adresa"));
	         c.setEmail(rowSet.getString("Email"));
	         c.setVarsta(rowSet.getInt("varsta"));
	      } catch (SQLException ex) {
	         ex.printStackTrace();
	      }
	      return c;
	   }
	}