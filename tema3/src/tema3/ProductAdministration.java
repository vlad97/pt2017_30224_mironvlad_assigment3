package tema3;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

import javax.sql.rowset.JdbcRowSet;

public class ProductAdministration {
	 private JdbcRowSet rowSet = null;
	   public ProductAdministration() {
	    	 rowSet=ConnectionFactory.createRowSet();
	         try {
				rowSet.setCommand("SELECT * FROM product");
				rowSet.execute();
			} catch (SQLException e) {
				e.printStackTrace();
			}
	   }
	   public Product create(Product p) {
	      try {
	         rowSet.moveToInsertRow();
	         //rowSet.updateInt("id", c.getId());
	         rowSet.updateString("pnume", p.getPnume());
	         rowSet.updateInt("cantitate", p.getCantitate());
	         rowSet.updateInt("pret", p.getPret());
	        
	         rowSet.insertRow();
	         rowSet.moveToCurrentRow();
	      } catch (SQLException ex) {
	         try {
	            rowSet.rollback();
	            p = null;
	         } catch (SQLException e) {

	         }
	         ex.printStackTrace();
	      }
	      return p;
	   }

	   public Product update(Product p) {
	      try {
	         rowSet.updateInt("id", p.getId());
	         rowSet.updateString("pnume", p.getPnume());
	         rowSet.updateInt("cantitate", p.getCantitate());
	         rowSet.updateInt("pret", p.getPret());
	         rowSet.updateRow();
	         rowSet.moveToCurrentRow();
	      } catch (SQLException ex) {
	         try {
	            rowSet.rollback();
	         } catch (SQLException e) {

	         }
	         ex.printStackTrace();
	      }
	      return p;
	   }

	   public void delete() {
	      try {
	         rowSet.moveToCurrentRow();
	         rowSet.deleteRow();
	      } catch (SQLException ex) {
	         try {
	            rowSet.rollback();
	         } catch (SQLException e) { }
	         ex.printStackTrace();
	      }

	   }

		public Product moveFirst() {
		      Product p = new Product();
		      try {
		         rowSet.first();
		         p.setId(rowSet.getInt("id"));
		         p.setPnume(rowSet.getString("pnume"));
		         p.setCantitate(rowSet.getInt("cantitate"));
		         p.setPret(rowSet.getInt("pret"));
		         

		      } catch (SQLException ex) {
		         ex.printStackTrace();
		      }
		      return p;
		   }
		

	   public Product moveLast() {
	      Product p = new Product();
	      try {
	         rowSet.last();
	         p.setId(rowSet.getInt("id"));
	         p.setPnume(rowSet.getString("pnume"));
	         p.setCantitate(rowSet.getInt("cantitate"));
	         p.setPret(rowSet.getInt("pret"));

	      } catch (SQLException ex) {
	         ex.printStackTrace();
	      }
	      return p;
	   }

	   public Product moveNext() {
	      Product p = new Product();
	      try {
	         if (rowSet.next() == false)
	            rowSet.previous();
	         p.setId(rowSet.getInt("id"));
	         p.setPnume(rowSet.getString("pnume"));
	         p.setCantitate(rowSet.getInt("cantitate"));
	         p.setPret(rowSet.getInt("pret"));

	      } catch (SQLException ex) {
	         ex.printStackTrace();
	      }
	      return p;
	   }

	   public Product movePrevious() {
	      Product p = new Product();
	      try {
	         if (rowSet.previous() == false)
	            rowSet.next();
	         p.setId(rowSet.getInt("id"));
	         p.setPnume(rowSet.getString("pnume"));
	         p.setCantitate(rowSet.getInt("cantitate"));
	         p.setPret(rowSet.getInt("pret"));

	      } catch (SQLException ex) {
	         ex.printStackTrace();
	      }
	      return p;
	   }

	   public Product getCurrent() {
	      Product p = new Product();
	      try {
	         rowSet.moveToCurrentRow();
	         p.setId(rowSet.getInt("id"));
	         p.setPnume(rowSet.getString("pnume"));
	         p.setCantitate(rowSet.getInt("cantitate"));
	         p.setPret(rowSet.getInt("pret"));
	      } catch (SQLException ex) {
	         ex.printStackTrace();
	      }
	      return p;
	   }

	public int getQuantity(int pId) {
		Connection conn= ConnectionFactory.getConnection();
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT cantitate FROM PRODUCT WHERE id="+pId);
			if (rs.next()) {
				  int quantity = rs.getInt("cantitate");
				  return quantity;
				}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return 0;
	}	

	public void setQuantity(int pId, int quantity) {
		Connection conn= ConnectionFactory.getConnection();
		String updateStatement =  "UPDATE PRODUCT SET CANTITATE = ? WHERE ID = ?";

		try {
			PreparedStatement preparedStatement = conn.prepareStatement(updateStatement);
			preparedStatement = conn.prepareStatement(updateStatement);
			preparedStatement.setInt(1, quantity);
			preparedStatement.setInt(2, pId);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			
		}	
	}
	
}
